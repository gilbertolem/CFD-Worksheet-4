#include "boundary_val.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void type_boundary(int f, int* B1, int* B2, int* B3, int* B4){
    /* 4: INFLOW    3: OUTFLOW  2: FREE-SLIP    1: NO-SLIP  */
     *B1 = (f >> 1)&1;
     *B2 = (f >> 2)&1;
     *B3 = (f >> 3)&1;
     *B4 = (f >> 4)&1;
}

void boundaryvalues(int imax, int jmax, double dx, double dy, double** restrict U,
    double** restrict V, int** flags, double** restrict Temp, char* problem){
    
    //********* Declare Auxilary Variables *********//
    int i,j, B_O, B_W, B_S, B_N, B_NO, B_NW, B_SO, B_SW, corner, f;
    double N_V, S_V, W_V, O_V, U_Inflow, V_Inflow, q;
    char N_T, S_T, W_T, O_T;
    int B1, B2, B3, B4, B5, BD, BN;
    spec_boundary_val( problem, &N_T, &N_V, &S_T, &S_V, &W_T, &W_V, &O_T, &O_V, &U_Inflow, &V_Inflow);

    //********* Set boundary conditions at the Domain Boundaries *********//
    j = 0; //South Boundary
    BD =(S_T=='D');
    BN = (S_T=='N');
	for(i=1;i<=imax;i++){

        //Set Boundary Values for U and V
        type_boundary(flags[i][j], &B1, &B2, &B3, &B4);
        U[i][0] = B4*U_Inflow + B3*U[i][1] + B2*U[i][1] - B1*U[i][1] ;
        V[i][0] = B4*V_Inflow + B3*V[i][1];
        //Set Boundary Values for Temp
        Temp[i][0] = BD*(2*S_V - Temp[i][1]) + BN*(Temp[i][1] + dy*S_V);

    }

    j = jmax+1; //North Boundary
    BD = (N_T=='D');
    BN = (N_T=='N');
	for(i=1;i<=imax;i++){
        //Set Boundary Values for U and V
        type_boundary(flags[i][j], &B1, &B2, &B3, &B4);
        U[i][jmax+1] = B4*U_Inflow + B3*U[i][jmax] + B2*U[i][jmax] - B1*U[i][jmax];
        V[i][jmax] = B4*V_Inflow + B3*V[i][jmax-1];
        //Set Boundary Values for Temp
        Temp[i][jmax+1] = BD*(2*N_V - Temp[i][jmax]) + BN*(Temp[i][jmax] + dy*N_V);
    }

    i = 0; //West Boundary
    BD = (W_T=='D');
    BN = (W_T=='N');
	for(j=1;j<=jmax;j++){
        type_boundary(flags[i][j], &B1, &B2, &B3, &B4);
        U[0][j] = B4*U_Inflow + B3*U[1][j];
        V[0][j] = B4*V_Inflow + B3*V[1][j] + B2*V[1][j] - B1*(V[1][j]);
        //Set Boundary Values for Temp
        Temp[0][j] = BD*(2*W_V - Temp[1][j]) + BN*(Temp[1][j] + dx*W_V);

    }

    i = imax+1; //East Boundary
    BD = (O_T=='D');
    BN = (O_T=='N');
	for(j=1;j<=jmax;j++){
        type_boundary(flags[i][j], &B1, &B2, &B3, &B4);
        U[imax][j] = B4*U_Inflow + B3*U[imax-1][j];
        V[imax+1][j] = B4*V_Inflow + B3*V[imax][j] + B2*V[imax][j] - B1*V[imax][j];
        //Set Boundary Values for Temp
        Temp[imax+1][j] = BD*(2*O_V - Temp[imax][j]) + BN*(Temp[imax][j] + dx*O_V);
    }

    //********* Set boundary conditions at the Inner Obstacles *********//

    for(i=1; i<=imax; i++)
    	for(j=1; j<=jmax; j++){
    		f = flags[i][j];
    		if (f%2==0){
        		B_O = (f >> 8)&1;
        		B_W = (f >> 7)&1;
        		B_S = (f >> 6)&1;
        		B_N = (f >> 5)&1;
                B_NO = B_N & B_O;
        		B_NW = B_N & B_W;
        		B_SO = B_S & B_O;
        		B_SW = B_S & B_W;
                //Set Adiabatic conditions for Temperature
                Temp[i][j] = B_N*Temp[i][j+1] + B_S*Temp[i][j-1] + B_W*Temp[i-1][j] + B_O*Temp[i+1][j]
                        - 0.5*B_NO*(Temp[i][j+1]+Temp[i+1][j]) - 0.5*B_NW*(Temp[i][j+1]+Temp[i-1][j])
                        - 0.5*B_SO*(Temp[i][j-1]+Temp[i+1][j]) - 0.5*B_SW*(Temp[i][j-1]+Temp[i-1][j]);
                //Set Velocity Non-Slip Conditions
        		corner = B_NO + B_NW + B_SO + B_SW;
                U[i][j]   = (!corner)*(-B_N*U[i][j+1] - B_S*U[i][j-1]) - B_NW*U[i][j+1] - B_SW*U[i][j-1];
                V[i][j]   = (!corner)*(-B_W*V[i-1][j] - B_O*V[i+1][j]) - B_SO*V[i+1][j] - B_SW*V[i-1][j];
                U[i-1][j] = (!corner)*(-B_N*U[i-1][j+1] - B_S*U[i][j-1]) - B_NO*U[i-1][j+1] - B_SO*U[i-1][j-1];
                V[i][j-1] = (!corner)*(-B_W*V[i-1][j-1] - B_O*V[i+1][j-1]) - B_NO*V[i+1][j-1] - B_NW*V[i-1][j-1];

	    	}
    	}
    
}

void boundaryvalues_fg(int imax, int jmax, double**U, double** V, double**F, double** G, int** flags){

  	//Set Boundary Condition at the Domain Boundaries
  	for(int j=1; j<=jmax; j++){
		F[0][j] = U[0][j];
		F[imax][j] = U[imax][j];
	}

	for(int i=1; i<=imax; i++){
		G[i][0] = V[i][0];
		G[i][jmax] = V[i][jmax];
	}

	//Set Boundary Conditions at the Obstacles
	int B_O, B_W, B_S, B_N, f;
    for(int i=1; i<=imax; i++)
    	for(int j=1; j<=jmax; j++){
    		f = flags[i][j];
    		if (f%2==0){
	    		B_O = (f >> 8)&1;
	    		B_W = (f >> 7)&1;
	    		B_S = (f >> 6)&1;
	    		B_N = (f >> 5)&1;
	    		F[i][j] = B_O*U[i][j];
	    		F[i-1][j] = B_W*U[i-1][j];
	    		G[i][j] = B_N*V[i][j];
	    		G[i][j-1]=B_S*V[i][j-1];
	    	}
	   	}

}

void spec_boundary_val(
		char* problem,
		char* N_T,
		double* N_V,
		char* S_T,
		double* S_V,
		char* W_T,
		double* W_V,
		char* O_T,
		double* O_V,
		double* U_inflow,
		double* V_inflow){

 if ( strcmp(problem, "heated-plate")==0 ){
    *N_T = 'N';
    *N_V = 0.0;
    *S_T = 'N';
    *S_V = 0.0;
    *W_T = 'N';
    *W_V = 0.0;
    *O_T = 'N';
    *O_V = 0.0;
    *U_inflow = 0.1;
    *V_inflow = 0.0;
 }
 else if( strcmp(problem, "convection")==0 ){
     *N_T = 'N';
     *N_V = 0.0;
     *S_T = 'N';
     *S_V = 0.0;
     *W_T = 'N';
     *W_V = 0.0;
     *O_T = 'N';
     *O_V = 0.0;
     *U_inflow = 0.0;
     *V_inflow = 0.0;
 }
 else if( strcmp(problem, "F1-heat-exchange")==0 ){
     *N_T = 'N';
     *N_V = 0.0;
     *S_T = 'N';
     *S_V = 0.0;
     *W_T = 'N';
     *W_V = 0.0;
     *O_T = 'N';
     *O_V = 0.0;
     *U_inflow = 5.0;
     *V_inflow = 0.0;
 }
 else if( strcmp(problem, "F2-heat-exchange")==0 ){
     *N_T = 'N';
     *N_V = 0.0;
     *S_T = 'N';
     *S_V = 0.0;
     *W_T = 'N';
     *W_V = 0.0;
     *O_T = 'N';
     *O_V = 0.0;
     *U_inflow = -5.0;
     *V_inflow = 0.0;
 }

}
