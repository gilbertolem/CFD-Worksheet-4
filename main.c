#include "helper.h"
#include "visual.h"
#include "init.h"
#include "sor.h"
#include <stdio.h>
#include"boundary_val.h"
#include "uvp.h"
#include "precice_adapter.h"
#include "adapters/c/SolverInterfaceC.h"


/**
 * The main operation reads the configuration file, initializes the scenario and
 * contains the main loop. So here are the individual steps of the algorithm:
 *
 * - read the program configuration file using read_parameters()
 * - set up the matrices (arrays) needed using the matrix() command
 * - create the initial setup init_uvp(), init_flag(), output_uvp()
 * - perform the main loop
 * - trailer: destroy memory allocated and do some statistics
 *
 * The layout of the grid is decribed by the first figure below, the enumeration
 * of the whole grid is given by the second figure. All the unknowns corresond
 * to a two dimensional degree of freedom layout, so they are not stored in
 * arrays, but in a matrix.
 *
 * @image html grid.jpg
 *
 * @image html whole-grid.jpg
 *
 * Within the main loop the following big steps are done (for some of the
 * operations a definition is defined already within uvp.h):
 *
 * - calculate_dt() Determine the maximal time step size.
 * - boundaryvalues() Set the boundary values for the next time step.
 * - calculate_fg() Determine the values of F and G (diffusion and confection).
 *   This is the right hand side of the pressure equation and used later on for
 *   the time step transition.
 * - calculate_rs()
 * - Iterate the pressure poisson equation until the residual becomes smaller
 *   than eps or the maximal number of iterations is performed. Within the
 *   iteration loop the operation sor() is used.
 * - calculate_uv() Calculate the velocity at the next time step.
 */

 typedef struct{
	double Re;                /* reynolds number   */
	double UI;                /* velocity x-direction */
	double VI;                /* velocity y-direction */
	double PI;                /* pressure */
	double GX;                /* gravitation x-direction */
	double GY;                /* gravitation y-direction */
	double t_end;             /* end time */
	double xlength;           /* length of the domain x-dir.*/
	double ylength;           /* length of the domain y-dir.*/
	double dt;                /* time step */
	double dx;                /* length of a cell x-dir. */
	double dy;                /* length of a cell y-dir. */
	int  imax;                /* number of cells x-direction*/
	int  jmax;                /* number of cells y-direction*/
	double alpha;             /* uppwind differencing factor*/
	double omg;               /* relaxation factor */
	double tau;               /* safety factor for time step*/
	int  itermax;             /* max. number of iterations  */
							   /* for pressure per time step */
	double eps;               /* accuracy bound for pressure*/
	double dt_value;          /* time for output */
	char FileName[256];       /* name of the file */
	char geometry[256];				/* geometry input file name */
	char problem[256];			 	/* spec boundary values */
	double TI;				   /*initial temperature */
	double Pr;			       /* Prandtl number */
	double beta;			   /* thermal expansion coeffienct */
    double x_origin;
    double y_origin;
    char precice_config[256];
    char participant_name[256];
    char mesh_name[256];
    char read_data_name[256];
    char write_data_name[256];
	int coupling_cells_count;  /*Will contain number of cells that need to be coupled via preCICE, to be used with preCICE api*/
}input_struct;

int main(int argn, char** args){

	/* declarations */
	input_struct in;
	double **U; //velocity in x–direction
	double **V; //velocity in y–direction
	double **P; //pressure
	double **RS; //right-hand side for pressure iteration
	double **F; double **G; //F, G
    double **Temp; //Temperature
	int xsize;
	int ysize;
	int **flags;
	int **pic;
	double t = 0; //time
	int n = 0; //iteration number

	/*choose scenario*/
	choose_problem(in.FileName, argn, args);	// Allows user to choose scenario for simulation
    printf("=================================================================\n");
    printf("\nSIMULATION PARAMETERS:\n\n");
    
    /*read parameters */
	(void)read_parameters(in.FileName,&in.Re,&in.UI,&in.VI,&in.PI,&in.GX,&in.GY,&in.t_end,&in.xlength,&in.ylength,
			&in.dt,&in.dx,&in.dy,&in.imax,&in.jmax,&in.alpha,&in.omg,&in.tau,&in.itermax,&in.eps,&in.dt_value, in.geometry, in.problem,
			&in.TI, &in.Pr, &in.beta, &in.x_origin, &in.y_origin, in.precice_config, in.participant_name, in.mesh_name, in.read_data_name,
            in.write_data_name);
    
    /*initialize preCICE*/
    precicec_createSolverInterface(in.participant_name, in.precice_config, 0, 1);
    int dim = precicec_getDimensions();

    /*save pgm into matrix, set xsize and ysize to values from file*/
	pic = read_pgm(in.geometry, &xsize, &ysize);

	/*creation and initialization (part1) of flags, also get number of coupling cells*/
	flags = init_flags_and_count_coupling(xsize, ysize, pic, &in.coupling_cells_count);

	/*check neighbours and set relevant fields in flags (initialization part 2)*/
	set_cell_neighbours(flags, xsize, ysize);

	/*check for illegal areas in mesh, double the resolution if illegal mesh detected */
	check_grid(pic, flags, xsize, ysize,&in.coupling_cells_count);

	/* overwrite imax and jmax based on file size; update dx and dy */
	in.imax = xsize - 2;
	in.jmax = ysize - 2;
	in.dx = in.xlength / (double)in.imax;
	in.dy = in.ylength / (double)in.jmax;

    /*define coupling mesh*/

    int meshID = precicec_getMeshID(in.mesh_name);

    int* vertexIDs = precice_set_interface_vertices(in.imax, in.jmax, in.dx, in.dy, in.x_origin, in.y_origin,
                                        in.coupling_cells_count, meshID, flags);

    // define Dirichlet part of coupling written by this solver
    int temperatureID = precicec_getDataID(in.write_data_name, meshID);
    double* temperatureCoupled = (double*) malloc(sizeof(double) * in.coupling_cells_count);

    // define Neumann part of coupling read by this solver
    int heatFluxID = precicec_getDataID(in.read_data_name, meshID);
    double* heatfluxCoupled = (double*) malloc(sizeof(double) * in.coupling_cells_count);

    // call precicec_initialize()
    double precice_dt = precicec_initialize();
    
    /*preallocate matrices*/
	U = matrix ( 0 , in.imax , 0 , in.jmax+1 );
	V = matrix ( 0 , in.imax+1 , 0 , in.jmax );
	P = matrix ( 0 , in.imax+1 , 0 , in.jmax+1 );
	RS = matrix ( 0 , in.imax+1 , 0 , in.jmax+1 );
	F  = matrix ( 0 , in.imax , 1 , in.jmax );
	G  = matrix ( 1 , in.imax , 0 , in.jmax );
    Temp = matrix ( 0 , in.imax+1 , 0 , in.jmax+1 );

	/*perform initialization of fields and save image*/
	init_uvp( in.UI, in.VI,  in.PI,  in.TI, in.imax, in.jmax,  U, V, P, flags, Temp);
    boundaryvalues(in.imax, in.jmax, in.dx, in.dy, U, V, flags, Temp, in.problem);
	char* output_name = initialize_output_folder(in.geometry, in.problem);
    write_vtkFile(output_name,n,in.xlength,in.ylength,in.imax,in.jmax,in.dx,in.dy,U,V,P,flags,Temp,in.x_origin,in.y_origin);
    // initialize data at coupling interface
    precice_write_temperature(in.imax, in.jmax, in.coupling_cells_count, temperatureCoupled, vertexIDs,
                                   temperatureID, Temp, flags);
    precicec_initialize_data(); // synchronize with OpenFOAM
    precicec_readBlockScalarData(heatFluxID, in.coupling_cells_count, vertexIDs, heatfluxCoupled); // read heatfluxCoupled

	/*now start the calculation: */
    printf("\n=================================================================\n");
    printf("\nCOMPUTING...\n");
    int it_prev = 0; //Start of the block of iterations where statistics are to be computed
    int itermax_stat = 0; //Number of times the iter_max is reached in SOR solver in iteration block
    double dt_stat = 0; //Statistic for the value of dt through the simulation
    double res_stat = 0; //Statistic for the value of the residual through the simulation

    while(precicec_isCouplingOngoing())
	{
		int it = 0; //current iteration number, for SOR solver
		double res = in.eps+1; //residual used in SOR solver, make it bigger than eps for first loop entry

		/*calculate timestep*/
		calculate_dt(in.Re, in.tau, &in.dt,	in.dx, in.dy,in.imax,in.jmax,U,V, in.Pr);
        in.dt = fmin(in.dt, precice_dt);
		t = t + in.dt;
        dt_stat += in.dt;
        
        /*set boundary condition:*/
		boundaryvalues(in.imax, in.jmax, in.dx, in.dy, U, V, flags, Temp, in.problem);
        set_coupling_boundary(in.imax, in.jmax, in.dx, in.dy, heatfluxCoupled, Temp, flags);

        /*calculate temperature*/
        calculate_temp(in.Re, in.Pr, in.alpha, in.dt, in.dx, in.dy, in.imax, in.jmax, U, V, Temp, flags );

		/*calculate F and G*/
		calculate_fg(flags, in.Re, in.GX, in.GY, in.alpha, in.beta, in.dt, in.dx, in.dy, in.imax, in.jmax, U, V, F, G, Temp);

		/*calculate rhs for pressure eq*/
		calculate_rs(in.dt,in.dx,in.dy,in.imax,in.jmax,F,G,RS);

		/*SOR iterative solver:*/
		while(it<in.itermax && res>in.eps)
		{
			sor(in.omg,in.dx,in.dy,in.imax,in.jmax,P,RS,&res, flags);
			it++;
		}
		if(it == in.itermax)
		{
			itermax_stat++;
		}
        res_stat += res;

		/*compute new U and V from the newly computed P now*/
		calculate_uv(flags, in.dt,in.dx,in.dy,in.imax,in.jmax,U,V,F,G,P);

        /*coupling*/
        precice_write_temperature(in.imax, in.jmax, in.coupling_cells_count, temperatureCoupled, vertexIDs,
                                       temperatureID, Temp, flags);
        precice_dt = precicec_advance(in.dt);
        precicec_readBlockScalarData(heatFluxID, in.coupling_cells_count, vertexIDs, heatfluxCoupled);

		/*record data for this timestep:*/
		if(fabs(fmod(t,in.dt_value)) < in.dt)
		{
            n++;
			write_vtkFile(output_name,n,in.xlength,in.ylength,in.imax,in.jmax,in.dx,in.dy,U,V,P,flags,Temp, in.x_origin, in.y_origin);
            printf("\nIteration Block %d-%d: time = %1.1f/%1.1f (%1.0f%% completed)\n", it_prev+1, n, t, in.t_end,t/in.t_end*100.0);
            dt_stat = dt_stat/(n-it_prev);
            res_stat = res_stat/(n-it_prev);
            if(itermax_stat) printf("\tWARNING: MAXIMAL ITERATIONS REACHED %d TIMES IN SOR\n",itermax_stat);
            printf("\tAverage timestep: %f",dt_stat);
            printf("\tAverage residual: %f\n", res_stat);
            dt_stat = 0;
            res_stat = 0;
            itermax_stat = 0;
            it_prev = n;
		}


	}

    printf("\n=================================================================\n");
    printf("SIMULATION ENDED SUCCESFULLY");
	printf("\n=================================================================\n");
    free_matrix ( U, 0 , in.imax , 0 , in.jmax+1 );
	free_matrix ( V, 0 , in.imax+1 , 0 , in.jmax);
	free_matrix ( P, 0 , in.imax+1 , 0 , in.jmax+1 );
	free_matrix ( RS, 0 , in.imax+1 , 0 , in.jmax+1 );
	free_matrix ( F, 0 , in.imax , 1 , in.jmax );
	free_matrix ( G, 1, in.imax, 0 , in.jmax);
    free_matrix ( Temp, 0, in.imax+1, 0 , in.jmax+1);
    free_flags(flags,xsize);
    free(temperatureCoupled);
    free(heatfluxCoupled);
    free(vertexIDs);

    precicec_finalize();
	return -1;
}
