#ifndef __RANDWERTE_H__
#define __RANDWERTE_H__


/**
 * The boundary values of the problem are set.
 */

void type_boundary(int f, int* B1, int* B2, int* B3, int* B4);
void boundaryvalues(int imax, int jmax, double dx, double dy, double** U, double** V, int** flags, double** Temp, char* problem);
void boundaryvalues_fg(int imax, int jmax, double**U, double** V, double**F, double** G, int** flags);
void spec_boundary_val(
		char* problem,
		char* N,
		double* N_v,
		char* S,
		double* S_v,
		char* W,
		double* W_v,
		char* O,
		double* O_v,
		double* U,
		double* V);
#endif
